using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CameraMovment : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 1000f;
    [SerializeField] private float rotationSpeed = 300f;

    private Vector2 _rotation = Vector2.zero;
    private Vector3 _moveVector = Vector3.zero;

    private Camera _camera;
    private Rigidbody _rigidbody;
    private ContainerDetector _detector;
    private InfoPanel _infoPanel;
    private YardController _controller;
    private AnimationController _animator;

    private bool _isInfo = false;

    private void Awake()
    {
        _controller = FindObjectOfType<YardController>();
        _animator = FindObjectOfType<AnimationController>();
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.useGravity = false;
        _camera = Camera.main;
        _detector = new ContainerDetector(_camera);
        _infoPanel = FindObjectOfType<InfoPanel>(true);

        Application.targetFrameRate = 120;        
    }

    private void Update()
    {
        if (Input.GetMouseButton(1))
        {
            Cursor.lockState = CursorLockMode.Locked;
            CameraControll();           
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            ContainerCreate();            
        }

        InfoPanel();
        Acceleration();
    }

    private IEnumerator TrackContainers()
    {
        Container currentContainer = null;

        while (_isInfo == true)
        {
            while (currentContainer == null)
            {
                _infoPanel.Hide();
                currentContainer = FindContainer();
                yield return null;

                if(_isInfo == false) yield break;
            }

            _infoPanel.Show(Input.mousePosition, currentContainer.ID);
            currentContainer = FindContainer();            

            yield return null;
        }

        _infoPanel.Hide();
    }

    private Container FindContainer()
    {
        Vector3 mousePos = Input.mousePosition;
        return _detector.DetectContainerByPointSreen(mousePos);
    }

    private bool _isInfoBuffer = false;

    private void InfoPanel()
    {
        if (Input.GetMouseButtonDown(1))
        {
            _isInfoBuffer = _isInfo;
            _isInfo = false;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            _isInfo = _isInfoBuffer;
            InfoStart();
        }
        else if (Input.GetKeyDown(KeyCode.I))
        {
            _isInfo = !_isInfo;
            InfoStart();
        }
    }

    private void InfoStart()
    {
        if (_isInfo == true) StartCoroutine(TrackContainers());
    }

    private void Acceleration()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift)) moveSpeed *= 2;
        else if (Input.GetKeyUp(KeyCode.LeftShift)) moveSpeed /= 2;
    }

    private void ContainerCreate()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.C))
        {
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                GameObject objecthit = hit.collider.gameObject;

                if (Input.GetKeyDown(KeyCode.C))
                {
                    if (objecthit.GetComponent<Platform>())                    
                        new ContainerCreator(_controller.Containers.Count, _animator).CreateContainer(objecthit.GetComponent<Platform>());                    
                    else if (objecthit.GetComponent<Container>())                    
                        new ContainerCreator(_controller.Containers.Count, _animator).CreateContainer(objecthit.GetComponent<Container>());
                }
                else
                {
                    Container container = objecthit.GetComponent<Container>();

                    if (container != null)
                        _animator.MoveSection(container.Section);                                       
                }
            }
        }
    }

    private void CameraControll()
    {
        _rotation += new Vector2(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X")) * Time.deltaTime;

        transform.eulerAngles = _rotation * rotationSpeed;

        var x = Input.GetAxis("Horizontal");
        var z = Input.GetAxis("Vertical");

        _moveVector = new Vector3(x, 0, z) * moveSpeed * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(1)) _rigidbody.velocity = transform.TransformDirection(_moveVector);
        else _rigidbody.velocity = Vector3.zero;
    }
}