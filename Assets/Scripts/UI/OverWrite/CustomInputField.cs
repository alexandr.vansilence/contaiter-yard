using UnityEngine.UI;
using UnityEngine;

public enum YardValuesType
{
    Length,
    Width,
    Height,
    LengthInterval,
    WidthInterval
}

public class CustomInputField : InputField
{
    [SerializeField]
    private YardValuesType typeYardValues = YardValuesType.Length;
    
    public YardValuesType TypeYardValues { get { return typeYardValues; } set { typeYardValues = value; } }
}
