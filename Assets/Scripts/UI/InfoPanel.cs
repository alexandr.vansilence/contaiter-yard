using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{
    private const float _offsetX = 100f;

    private Text _id;

    private void Awake()
    {
        _id = GetComponentInChildren<Text>(true);
        gameObject.SetActive(false);  
    }

    public void Show(Vector2 screenPosition, string id)
    {
        _id.text = id;

        var test = screenPosition + Vector2.right * _offsetX;

        transform.position = test;

        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}