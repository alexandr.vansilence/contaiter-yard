using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartWindow : MonoBehaviour
{
    private GameObject _mainPanel;

    public Color colorPlatforms = Color.white;

    public readonly Dictionary<YardValuesType, CustomInputField> _inputData = new Dictionary<YardValuesType, CustomInputField>();
    public Button button;

    private void Awake()
    {        
        _mainPanel = gameObject;
        CustomInputField[] _inputFields = _mainPanel.GetComponentsInChildren<CustomInputField>();

        for (int i = 0; i < _inputFields.Length; i++) _inputData.Add(_inputFields[i].TypeYardValues, _inputFields[i]); 
    }

    private void Start()
    {
        button.onClick.AddListener(ClosePanel);
    }

    private void ClosePanel()
    {
        _mainPanel.SetActive(false);
    }  
}