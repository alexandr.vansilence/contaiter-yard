using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animator
{
    private enum MovingType { Down = -1, Up = 1 }

    private readonly float _heigthShift = 4f;
    private readonly float _animationTime = 1f;
    private readonly float _dalay�ontainer = 0.1f;
    private readonly float _animationPercent = 0.35f;
    private int _sectionInAir = -1;

    [HideInInspector] public readonly bool[] IsPlayingSections;

    private readonly YardController _controller;
    private readonly Dispatcher _dispatcher;
    private static Animator _instance;

    public int SectionInAir { get { return _sectionInAir; } }  

    public static Animator Instance
    {
        get
        {
            if (_instance == null) _instance = new Animator();

            return _instance;
        }
    }

    private Animator()
    {
        _controller = Object.FindObjectOfType<YardController>();
        _dispatcher = Object.FindObjectOfType<Dispatcher>();
        IsPlayingSections = new bool[_controller.Containers.Count];

        for (int i = 0; i < IsPlayingSections.Length; i++) IsPlayingSections[i] = false;
    }

    public void ContainerShowController(int section)
    {
        if (_sectionInAir > 0 && IsPlayingSections[_sectionInAir] == true) return;

        Container maxContainer = MaxHeigthContainer(section);

        if (maxContainer.Floor > 0 && maxContainer.IsInAir == false) ContainersUp(section, maxContainer.Floor);        
        else if (maxContainer.IsInAir == true) ContainersDown(section, maxContainer.Floor);         
    }

    private void ContainersUp(int section, int maxFloor)
    {     
        if (_sectionInAir != -1) ContainerShowController(_sectionInAir);

        IsPlayingSections[section] = true;
        _sectionInAir = section;
         MoveNextContainerFloor(section, maxFloor, MovingType.Up, 1);        
    }

    private void ContainersDown(int section, int maxFloor)
    {
        IsPlayingSections[section] = true;
        MoveNextContainerFloor(section, 1, MovingType.Down, maxFloor);
        _sectionInAir = -1;
    }

    private Container MaxHeigthContainer(int section)
    {
        int currentRow = 0;

        for (int row = 0; row < _controller.Containers[section].Count; row++)        
            if (_controller.Containers[section][currentRow].Count < _controller.Containers[section][row].Count)
                currentRow = row;        

        return _controller.Containers[section][currentRow][^1];
    }

    private IEnumerator MoveContainer(int section, Container[] containers, int endFloor, MovingType moveDir)
    {
        yield return new WaitForSeconds(_dalay�ontainer);
        MoveNextContainerFloor(section, containers[0].Floor - (int)moveDir, moveDir, endFloor);
        Vector3[] endPoints = new Vector3[containers.Length];

        for (int i = 0; i < containers.Length; i++) endPoints[i] = EndPosition(containers[i], moveDir);

        float t = 0;

        while (t < _animationPercent)
        {
            for (int i = 0; i < containers.Length; i++)            
                containers[i].Position = Vector3.Lerp(containers[i].Position, endPoints[i], t);             

            t += Time.deltaTime / _animationTime;
            yield return null;
        }

        for (int i = 0; i < containers.Length; i++) containers[i].Position = endPoints[i];

        if (containers[0].Floor == endFloor) IsPlayingSections[section] = false;        
    }

    private Vector3 EndPosition(Container container, MovingType direction)
    {
        return new Vector3(container.Position.x, container.Position.y + (int)direction * (_heigthShift * container.Floor), container.Position.z); 
    }

    private void MoveNextContainerFloor(int section, int floor, MovingType move, int endFloor)
    {
        Container[] containers = FindContainersOnFloor(section, floor);

        bool isUp = move == MovingType.Up;

        if (containers.Length > 0 && containers[0].Floor > 0)
        {
            for (int i = 0; i < containers.Length; i++)            
                containers[i].IsInAir = isUp;
            
            _dispatcher.StartCoroutine(MoveContainer(section, containers, endFloor, move));
        }
    }

    private Container[] FindContainersOnFloor(int section, int floor)
    {
        List<Container> containersOnFloor = new List<Container>();

        for (int row = 0; row < _controller.Containers[section].Count; row++)        
            if (_controller.Containers[section][row].Count > floor && _controller.Containers[section][row][floor].Floor == floor)            
                containersOnFloor.Add(_controller.Containers[section][row][floor]);
                   
        return containersOnFloor.ToArray();
    }
}