using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public AnimatorSM[] SectionAnimators;

    public AnimatorSM SelectedSection = null;

    private void Awake()
    {
        int quantity = FindObjectOfType<YardController>().Quantity;
        SectionAnimators = new AnimatorSM[quantity];

        for (int i = 0; i < quantity; i++) SectionAnimators[i] = new AnimatorSM(new AnimationGround(), i, this);
    }

    public void MoveSection(int sectionNum)
    {
        SectionAnimators[sectionNum].Moving();
    }
}
