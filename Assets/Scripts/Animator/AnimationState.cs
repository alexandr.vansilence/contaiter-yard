using UnityEngine;

public abstract class AnimationState 
{
    protected AnimatorSM _animator;
    protected readonly YardController _controller;

    public AnimatorSM Animator { set { _animator = value; } }

    protected AnimationState()
    {
        _controller = Object.FindObjectOfType<YardController>();
    }

    public abstract void Moving();
}