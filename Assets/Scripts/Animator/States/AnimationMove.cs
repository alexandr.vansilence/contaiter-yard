using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationMove : AnimationState
{
    protected enum MovingType { Down = -1, Up = 1 }

    private readonly float _heigthShift = 4f;
    private readonly float _animationTime = 1f;
    private readonly float _dalay�ontainer = 0.1f;
    private readonly float _animationPercent = 0.35f;

    protected readonly Dispatcher _dispatcher;
    protected readonly int startFloor;
    protected readonly int endFloor;   

    public override void Moving() { }

    protected AnimationMove(int startFloor, int endFloor) : base()
    {
        _dispatcher = Object.FindObjectOfType<Dispatcher>();

        this.startFloor = startFloor;
        this.endFloor = endFloor;
    }

    private IEnumerator MoveContainer(int section, Container[] containers,  MovingType moveDir)
    {
        yield return new WaitForSeconds(_dalay�ontainer);

        MoveNextContainerFloor(section, containers[0].Floor - (int)moveDir, moveDir);

        Vector3[] endPoints = new Vector3[containers.Length];

        for (int i = 0; i < containers.Length; i++) endPoints[i] = EndPosition(containers[i], moveDir);

        float t = 0;

        while (t < _animationPercent)
        {
            for (int i = 0; i < containers.Length; i++)
                containers[i].Position = Vector3.Lerp(containers[i].Position, endPoints[i], t);

            t += Time.deltaTime / _animationTime;
            yield return null;
        }

        for (int i = 0; i < containers.Length; i++) containers[i].Position = endPoints[i];

        if (containers[0].Floor == endFloor)
        {
            if (moveDir == MovingType.Up) _animator.ChangeState(new AnimationAir());
            else if (moveDir == MovingType.Down) _animator.ChangeState(new AnimationGround());
        } 
    }

    private Vector3 EndPosition(Container container, MovingType direction)
    {
        return new Vector3(container.Position.x, container.Position.y + (int)direction * (_heigthShift * container.Floor), container.Position.z);
    }

    protected void MoveNextContainerFloor(int section, int floor, MovingType move)
    {
        Container[] containers = FindContainersOnFloor(section, floor);

        bool isUp = move == MovingType.Up;

        if (containers.Length > 0 && containers[0].Floor > 0)
        {
            for (int i = 0; i < containers.Length; i++)
                containers[i].IsInAir = isUp;

            _dispatcher.StartCoroutine(MoveContainer(section, containers, move));
        }
    }

    private Container[] FindContainersOnFloor(int section, int floor)
    {
        List<Container> containersOnFloor = new List<Container>();

        for (int row = 0; row < _controller.Containers[section].Count; row++)        
            if (_controller.Containers[section][row].Count > floor && _controller.Containers[section][row][floor].Floor == floor)            
                containersOnFloor.Add(_controller.Containers[section][row][floor]);        
        
        return containersOnFloor.ToArray();
    }

    protected void StartAnimation(MovingType movingType)
    {
        MoveNextContainerFloor(_animator.Section, startFloor, movingType);
    }
}