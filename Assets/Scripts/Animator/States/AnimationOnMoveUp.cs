using System.Collections;

public class AnimationOnMoveUp : AnimationMove
{
    public AnimationOnMoveUp(int startFloor) : base(startFloor, 1) { }

    public override void Moving()
    {
        if (_animator.IsMoving == true) return;

        _animator.IsMoving = true;
        _animator.SelectedSection = _animator;
        StartAnimation(MovingType.Up);                       
    }
}