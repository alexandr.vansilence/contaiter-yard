public class AnimationGround : AnimationStay
{
    public override void Moving()
    {
        if (_animator.SelectedSection != null && _animator.SelectedSection.IsMoving) return; 

        int maxFloor = MaxFloorContainer(_animator.Section);

        if (maxFloor > 0)
        {
            if (_animator.SelectedSection != null) _animator.SelectedSection.Moving();

            _animator.ChangeState(new AnimationOnMoveUp(maxFloor));
            _animator.Moving();            
        }
    }
}