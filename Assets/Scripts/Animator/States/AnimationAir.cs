public class AnimationAir : AnimationStay
{
    public override void Moving()
    {
        _animator.ChangeState(new AnimationOnMoveDown(MaxFloorContainer(_animator.Section)));
        _animator.Moving();
    }
}