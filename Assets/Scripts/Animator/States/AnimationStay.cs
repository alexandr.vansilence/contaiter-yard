using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStay : AnimationState
{
    protected AnimationStay() { }

    public override void Moving() { }

    protected int MaxFloorContainer(int section)
    {
        int currentRow = 0;
        try
        {
           

            for (int row = 0; row < _controller.Containers[section].Count; row++)
                if (_controller.Containers[section][currentRow].Count < _controller.Containers[section][row].Count)
                    currentRow = row;

            return _controller.Containers[section][currentRow].Count - 1;
        }
        catch (System.Exception e)
        {
            Debug.LogException(e);

            Debug.Log(section);
            Debug.Log(currentRow);
            return 0;
        }

    }    
}

