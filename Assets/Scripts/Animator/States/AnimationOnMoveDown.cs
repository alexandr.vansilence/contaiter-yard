public class AnimationOnMoveDown : AnimationMove
{
    public AnimationOnMoveDown(int endFloor) : base(1, endFloor) { }

    public override void Moving()
    {
        if (_animator.IsMoving == true) return;

        _animator.IsMoving = true;
        StartAnimation(MovingType.Down);
    }   
}