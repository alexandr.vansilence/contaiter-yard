public class AnimatorSM
{
    private AnimationState _state;

    public readonly int Section;
    public AnimationController ControllerAnimation;
    public bool IsMoving;

    public AnimatorSM SelectedSection
    {
        get { return ControllerAnimation.SelectedSection;  }
        set { ControllerAnimation.SelectedSection = value; }
    }

    public AnimatorSM(AnimationState initialState, int section, AnimationController animationController)
    {
        Section = section;
        _state = initialState; 
        ControllerAnimation = animationController;
    }

    public void ChangeState(AnimationState state)
    {
        if (_state != state)
        {
            _state = state;

            if (state is AnimationStay) IsMoving = false;

            if (state is AnimationGround  && Section == SelectedSection.Section) SelectedSection = null;            
        }
    }

    public void Moving()
    {
        if (_state != null)
        {
            _state.Animator = this;
            _state.Moving();           
        }
    }
}