using System.Collections.Generic;
using UnityEngine;

public class YardCreator : CubeCreator
{
    private const float _platformHeight = 0.01f;
    private const float _planeToCubeSize = 0.1f;

    private GameObject[,] _platforms;
    private ContainerParams _containerParams;
    private YardIntervals _intervals;
    private Vector3 _positionYard;
    private Color _platformColor;
    private Color _yardColor = new Color(0.2f, 0.2f, 0.2f, 1);

    public YardCreator(Vector3 positionYard, ContainerParams containerParams, YardIntervals intervals, int quantity, Color colorPlatforms) : base(quantity)
    {
        _positionYard = positionYard;
        _containerParams = containerParams;
        _intervals = intervals;
        _platformColor = colorPlatforms;
    }

    public void CreateYard()
    {
        GameObject yard = GameObject.CreatePrimitive(PrimitiveType.Plane);        

        Material material = yard.GetComponent<Renderer>().material;
        material.color = _yardColor;
        SetSmoothness(material, 0.01f);

        yard.transform.localScale = new Vector3(PositionCalc(_containerParams.Length, _intervals.LengthInterval, _quantity, _planeToCubeSize), 1, PositionCalc(_containerParams.Width, _intervals.WidthInterval, _quantity, _planeToCubeSize));
        yard.name = "Yard";

        CreatePlatforms();
    }

    private void CreatePlatforms()
    {
        _platforms = new GameObject[_quantity, _quantity];
        Vector3 shift = Vector3.zero;

        ContainerParams platformParams = new ContainerParams(_containerParams.Length, _containerParams.Width, _platformHeight);

        float lengthRange = platformParams.Length + _intervals.LengthInterval;
        float widthRange  = platformParams.Width  + _intervals.WidthInterval ;

        for (int i = 0; i < _quantity; i++)
        {
            shift.x = i * lengthRange;

            _controller.Containers.Add(new List<List<Container>>());

            for (int j = 0; j < _quantity; j++)
            {
                shift.z = j * widthRange;
                 
                _platforms[i, j] = CreatePlatform(i, j, platformParams, shift);

                _controller.Containers[i].Add(new List<Container>());
            }            
        }
    }

    private GameObject CreatePlatform(int i, int j, ContainerParams platformParams, Vector3 shift)
    {       
        Vector3 centerPlatform = -_positionYard - shift;
        int quantity = _quantity - 1;
        Vector3 position = new Vector3(centerPlatform.x + PositionCalc(platformParams.Length, _intervals.LengthInterval, quantity, 0.5f), 0, centerPlatform.z + PositionCalc(platformParams.Width, _intervals.WidthInterval, quantity, 0.5f));
        GameObject platform = CreateCube(position, platformParams, $"platform ({i},{j})");

        Platform platformP = platform.AddComponent<Platform>();
        platformP.Section = i;
        platformP.Row = j;
        platformP.HeightPlatform = _platformHeight;
        platformP.ContainerParams = _containerParams;

        Material material = platform.GetComponent<Renderer>().material;
        material.color = _platformColor;

        SetSmoothness(material, 0.2f);

        return platform;
    }

    protected float PositionCalc(float value, float interval, int quantity, float coefficient = 1)
    {
        return (value + interval) * quantity * coefficient;
    }

    private void SetSmoothness(Material material, float value)
    {
        material.SetFloat("_Glossiness", value);
    }
}