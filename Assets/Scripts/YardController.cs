using System.Collections.Generic;
using UnityEngine;

public class YardController : MonoBehaviour
{
    private StartWindow _view;
    private YardCreator _model;

    public List<List<List<Container>>> Containers = new List<List<List<Container>>>();
    public readonly int Quantity = 10;

    public void CreateYard()
    {
        ContainerParams containerParams = new ContainerParams(ParseNumber(_view._inputData[YardValuesType.Length]), ParseNumber(_view._inputData[YardValuesType.Width]), ParseNumber(_view._inputData[YardValuesType.Height]));
        YardIntervals intervals = new YardIntervals(ParseNumber(_view._inputData[YardValuesType.LengthInterval]), ParseNumber(_view._inputData[YardValuesType.WidthInterval]));

        _model = new YardCreator(Vector3.zero, containerParams, intervals, Quantity, _view.colorPlatforms);
        _model.CreateYard();
    }

    private void Awake()
    {
        _view = FindObjectOfType<StartWindow>();
        _view.button.onClick.AddListener(CreateYard);
    }

    private static float ParseNumber(CustomInputField field)
        => float.Parse(field.text.Replace('.', ','));
}