using System;
using UnityEngine;

public class ContainerCreator : CubeCreator
{
    private AnimationController _animator;

    public ContainerCreator(int quantity, AnimationController animator) : base(quantity)
    {
        _animator = animator;
    }

    public void CreateContainer(IYardElement yardElement)
    {
        IYardElement element = yardElement;

        if (_animator.SelectedSection != null && element.Section == _animator.SelectedSection.Section)
        {
            _animator.MoveSection(_animator.SelectedSection.Section);
            return;
        }
        else if (element.IsCanCreate == false)
        {
            element = GetUpperElement(element.Section, element.Row);
        }        
                
        Vector3 centerPosition;
        float heightShift;
        int floor;

        if (element is Platform platform)
        {
            centerPosition = platform.Position;
            heightShift = platform.HeightPlatform;
            floor = 0;
        }
        else if(element is Container container)
        {
            centerPosition = container.Position;
            heightShift = container.ContainerParams.Height * 0.5f;
            floor = container.Floor + 1;
        }
        else
        {
            throw new NotImplementedException("Element is not correct");
        }
        
        centerPosition.y += heightShift + (element.ContainerParams.Height * 0.5f);

        GameObject containerMesh = CreateCube(centerPosition, element.ContainerParams, $"container ({element.Row},{element.Section})");

        Container newContainer = containerMesh.AddComponent<Container>();
        newContainer.Section = element.Section;
        newContainer.Row = element.Row;
        newContainer.Floor = floor;
        newContainer.ContainerParams = element.ContainerParams;

        Material material = containerMesh.GetComponent<Renderer>().material;
        material.color = new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);

        element.IsCanCreate = false;

        _controller.Containers[newContainer.Section][newContainer.Row].Add(newContainer);
    }

    private IYardElement GetUpperElement(int section, int row)
    {
        for (int floor = 0; floor < _controller.Containers[section][row].Count; floor++)
        {
            if(_controller.Containers[section][row][floor].IsCanCreate == true) return _controller.Containers[section][row][floor];
        }

        return null;
    }
}