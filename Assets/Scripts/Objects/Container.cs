using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BoxCollider))]
public class Container : MonoBehaviour, IYardElement
{
    private string _id = string.Empty;

    public int Section { get; set; }
    public int Row { get; set; }
    public int Floor { get; set; }

    public string ID
    {
        get 
        {
            if(_id == string.Empty)
            {
                _id = $"S{Section}R{Row}F{Floor}";
            }

            return _id;
        }
    }

    public ContainerParams ContainerParams { get; set; }

    public bool IsCanCreate { get; set; } = true;
    public bool IsInAir { get; set; } = false;

    public Vector3 Position
    {
        get { return gameObject.transform.position; }
        set { gameObject.transform.position = value; }
    }

    public void DestroyContainer()
    {
        Destroy(gameObject);
    }
}