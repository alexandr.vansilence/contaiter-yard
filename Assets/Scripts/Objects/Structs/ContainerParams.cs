public struct ContainerParams
{
    public float Length { get; private set; }
    public float Width { get; private set; }
    public float Height { get; private set; }
    public ContainerParams(float length, float width, float height)
    {
        Length = length;
        Width = width;
        Height = height; 
    }
}

public struct YardIntervals
{
    public float LengthInterval { get; private set; }
    public float WidthInterval { get; private set; }
    public YardIntervals(float lengthInterval, float widthInterval)
    {
       LengthInterval = lengthInterval;
        WidthInterval =  widthInterval;
    }
}