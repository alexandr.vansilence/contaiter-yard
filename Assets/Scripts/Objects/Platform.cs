using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Platform : MonoBehaviour, IYardElement
{
    public int Section { get; set; }
    public int Row { get; set; }
    public float HeightPlatform { get; set; }

    public ContainerParams ContainerParams { get; set; }

    public Vector3 Position => transform.position;

    public bool IsCanCreate { get; set; } = true;
}