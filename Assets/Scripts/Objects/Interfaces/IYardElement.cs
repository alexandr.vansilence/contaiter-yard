public interface IYardElement 
{
    public int Section { get; set; }
    public int Row { get; set; }

    public ContainerParams ContainerParams { get; set; }

    public bool IsCanCreate { get; set; }
}
