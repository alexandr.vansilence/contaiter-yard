using UnityEngine;

public class CubeCreator  
{
    protected int _quantity;

    protected YardController _controller;

    protected CubeCreator(int quantity)
    {
        _quantity = quantity;
        _controller = Object.FindObjectOfType<YardController>();
    }

    protected GameObject CreateCube(Vector3 position, ContainerParams parametrs, string name = "cube")
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.name = name;
        cube.transform.localScale = new Vector3(parametrs.Length, parametrs.Height, parametrs.Width);        
        cube.transform.position = position;
        return cube;
    }   
}