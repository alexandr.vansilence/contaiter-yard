using UnityEngine;

public class ContainerDetector
{
    private Camera _camera;

    public ContainerDetector(Camera camera) => _camera = camera;

    public Container DetectContainerByPointSreen(Vector3 screenPosition)
    {
        Ray ray = _camera.ScreenPointToRay(screenPosition);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            return hit.collider.GetComponent<Container>();
        }

        return null;
    }
}