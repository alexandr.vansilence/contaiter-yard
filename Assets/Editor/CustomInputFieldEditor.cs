using UnityEditor;
using UnityEditor.UI;

[CustomEditor(typeof(CustomInputField))]
public class CustomInputFieldEditor : InputFieldEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        CustomInputField editor = (CustomInputField)target;
        editor.TypeYardValues = (YardValuesType)EditorGUILayout.EnumPopup(editor.TypeYardValues);
    }
}